<?php 
include('restrict.php');
include('header.php');
?>

          <h2 class="sub-header">Add Vendors</h2>

          <div class="table-responsive">

<?php
if( isset( $_POST['submit']) ) {
include_once('inc/db.php');

// Get values from form
$business = $_POST['business'];
$phone    = $_POST['phone'];
$hours    = $_POST['hours'];
$address  = $_POST['address'];
$note     = $_POST['note'];

// Insert data into mysql
$sql = "INSERT INTO cgvendors ( business, phone, hours, address, note ) 
VALUES ( :business, :phone, :hours, :address, :note )";

$stmt = $dbh->prepare($sql);

$stmt->bindValue(':business', $business);
$stmt->bindValue(':phone',    $phone);
$stmt->bindValue(':hours',    $hours);
$stmt->bindValue(':address',  $address);
$stmt->bindValue(':note',     $note);

$stmt->execute();

    //Execute the statement and insert the new account.
    //If the process is successful.
    if($stmt){

        echo "<br>Information UPDATED to system Successfully!";
        echo "<BR>";
        echo "Data entered - "; 
        $source = $dateformat;
        $date = new DateTime($source);
        echo $date->format('m-d-Y H:m');
echo "<hr><p><a class='btn btn-primary' href='index.php'>BACK TO ADMIN</a></p>
<p>You add another vendor now or return to admin with link above.</p>
<p><a class='btn btn-primary' href='vendors-template.php'>ADD CONTACT</a></p>"; 
    
        // throw errors if not success
        } else {
            print "oops This vendor did not process correctly, please try again.";
            echo $sql . "<br>" . $dbh->error;
            }
}
?>
<style>table input, table textarea {margin: 3px 0;} table tr td:first-child{width:15%;} table tr td:nth-of-type(2){width:60%;}</style>
<form name="form1" action="vendors-template.php" method="post">
<table><tbody class="col-lg-8 col-md-12">
<tr valign="bottom">
<td><label>Business: </label></td><td><input name="business" class="form-control" type="text" /></td><td></td></tr>
<tr valign="bottom">
<td><label>Phone: </label></td><td><input name="phone" class="form-control" type="text" /></td><td></td></tr>
<tr valign="bottom">
<td><label>Hours: </label></td><td><textarea name="hours" class="form-control" rows="6" cols="32"></textarea></td><td></td></tr>
<tr valign="bottom">
<td><label>Address: </label></td><td><input name="address" class="form-control" type="text" /></td><td></td></tr>
<tr valign="bottom">
<td><label>Note: </label></td><td><input name="note" class="form-control" type="text" /></td><td></td></tr>
<tr valign="bottom">

<td><input type="submit" name="submit" class="btn btn-primary" value="Submit" /></td></tr>
</tbody></table></form>

    
    </div><!-- ends tables responsive -->
<?php include('footer.php'); ?>