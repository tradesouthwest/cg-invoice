<?php

$comp_name = "AZ Widget Builders";                                       //Company name
$comp_moniker = "Widget Builders and Contractors";                      // moniker name
$comp_addr   = "515 E. Carefree Hwy. #722 ";                           // Address
$comp_city   = "Phoenix, AZ 85085";                                   // City St., Zip
$comp_phone = "(602) 555-3333";                                      // Phone
$comp_slogan = "The smart way to get a professional installation";  // slogan
$comp_payUrl = "http://thenameofyourwebsite.com/paystation.html";  // Payment portal URL
$comp_payquest = "(602) 555-2222";                                //Questions about bill info
$comp_email = "sales@thenameofyourwebsite.com";                  // Emails
$disclaimer = "Thank You For Your Business!";                   // disclaimer or greeting footer of invoice
$mytime_zone = "MST";             // Set default time zone - visit php.net/manual/en/timezones.php for ref.

?> 
