<?php 
include('restrict.php');
include('header.php');
?>

    <h2 class="sub-heading">New Invoice</h2>
        <div class="table-responsive" id="new_invoice">
<?php
// invoice
// TSW =|=

include('print-settings.php');
?>
<br /><!--pixels at 300dpi is 612 by 792 which equals 8.5" by 11" -->

<form action="invoice.php" name="new_invoice" method="POST">
    <table id="table"><tbody>
    <tr>
    <td><h4><?php echo $comp_name; ?><br><?php echo $comp_moniker; ?><br><?php echo $comp_addr; ?><br>
    <?php echo $comp_city; ?><br><php echo comp_phone; ?></h4></td>
    <td><img alt="logo only shows on print" src="images/nologo.svg" border="0" />
    <p><?php echo $comp_slogan; ?></p></td>
    </tr>

    <tr class="marg-tm1"><td>&nbsp;</td></tr>
    <tr>
    <td>
    <p><label>Name:</label><br>
        <input class="form-control"  name="name" type="text" /></p>
    <p><label>Phone:</label><br>
        <input class="form-control"  name="phone" type="text" /><br>
    <p><label>Address:</label><br>
        <textarea rows="4" cols="35" name="address"></textarea>
    </td>
    <td>
    <p><label>Date:</label><br>
        <input class="form-control" name="date" type="date" />
    <p><label>Invoice Num.:</label><br>
        <input class="form-control" name="invnum" type="text" /></p>
    <p><label>Customer ID:</label><br>
        <input class="form-control" name="cust" type="text" /></p>
    <p><label>Job Type:</label><br>
        <input class="form-control" name="model" type="text" /></p></td>
    </tr>


        <tr>
            <td id="midtext"><label>Description</label><br>
                <textarea rows="9" cols="35" name="work" /></textarea> 
            <br>
            <p id="payonline">Pay online: <br> <?php echo $comp_payUrl; ?></p></td>

            <td><p><label>Sub Total:</label><br>
                <input class="form-control" name="sub" type="text" /></p>
            <p><label>Tax</label><br>
                <input class="form-control" name="tax" type="text" /></p>
            <p><label>Total: </label><br>
                    <input class="form-control" type="text" name="total" /></p>
        </td>
        </tr>

            <tr><td colspan=2><p><label>Payment Notes: </label><br>
                    <input class="form-control" type="text" name="paid" /></p></td></tr>
            <tr><td colspan="2"><div class="tiny"><small><hr></small></div></td><tr>
            <tr><td colspan=2>For questions about this invoice, please contact: <?php echo $comp_payquest; ?> <?php echo $comp_email; ?></td>            
            </tr>
          

               
                <tr><td><label>Save Invoice</label><p style="width: 100px"> &nbsp; <input type="hidden" name="comment" value=0 />
                    <input class="btn btn-success btn-lg" type="submit" name="submit" value="Submit" /></p></td>
                </tr>
               

</tbody></table><!-- ends table div --></form><br><hr><br>
        </div>
<?php include('footer.php'); ?>