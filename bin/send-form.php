<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="private restricted no follow">
    <title>Invoicing | Invoice</title>
    <style type="text/css">  body { margin: .6cm; }</style>
    </head>
    <body>
<?php 
if (isset( $_GET['id'] )) {
$id = $_GET['id'];

include('../inc/db.php');

    // Retrieve data from database
    $sql = ("SELECT * FROM cginvoice WHERE id = :id");
        $stmt = $dbh->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($rows as $row) {
            $name   = $row['name'];
            $phone  = $row['phone'];
            $datex  = $row['date'];
            $invnum = $row['invnum'];
            $cust   = $row['cust'];
            $model  = $row['model'];
            $work   = $row['work']; 
            $sub    = $row['sub'];
            $tax    = $row['tax']; 
            $total  = $row['total'];
        } 
$dbh = null;
?>

<form name="sendform" action="send-form.php" method="POST">
<p><label>To</label><br><input type="email" name="emailto" class="form-control"/></p>
<p><label>Subject</label><br><input type="text" name="subject" class="form-control"/></p>
<input type="hidden" name="name" value="<?php echo $name; ?>">
<input type="hidden" name="phone" value="<?php echo $phone; ?>">
<input type="hidden" name="datex" value="<?php echo $datex; ?>">
<input type="hidden" name="invnum" value="<?php echo $invnum; ?>">
<input type="hidden" name="cust" value="<?php echo $cust; ?>">
<input type="hidden" name="model" value="<?php echo $model; ?>">
<input type="hidden" name="work" value="<?php echo $work; ?>">
<input type="hidden" name="sub" value="<?php echo $sub; ?>">
<input type="hidden" name="tax" value="<?php echo $tax; ?>">
<input type="hidden" name="total" value="<?php echo $total; ?>">
<input type="submit" class="btn btn-success" name="sendmail">
<br><hr><br>
<?php
echo $id;
}
?>
<hr>
<?php
if (isset( $_POST['sendmail'] )) {
include('../print-settings.php');
//define the receiver of the email  
$to      = $_POST['emailto'];
$subject = $_POST['subject'];
$from    = $comp_email;

$name   = $_POST['name'];
$phone  = $_POST['phone'];
$datex  = $_POST['date'];
$invnum = $_POST['invnum'];
$cust   = $_POST['cust'];
$model  = $_POST['model'];
$work   = $_POST['work']; 
$sub    = $_POST['sub'];
$tax    = $_POST['tax']; 
$total  = $_POST['total'];

$content = "<html><head><title>HTML email</title></head><body>";
$content .="<h4>" . strip_tags($comp_name) . "</h4>";
$content .="<h4>" . strip_tags($comp_moniker) . "</h4>";
$content .="<h4>" . strip_tags($comp_addr) . "</h4>";
$content .="<h4>" . strip_tags($comp_city) . "</h4>";
$content .="<p>" . strip_tags($comp_phone) . "</p>";
$content .="<h5>" . strip_tags($name) . "</h5>";
$content .="<h5>" . strip_tags($address) . "</h5>";
$content .="<p>" . strip_tags($phone) . "</p>";
$content .="<p>" . strip_tags($datex) . "</p>";
$content .="<p>Invoice " . strip_tags($invnum) . "</p>";
$content .="<p>Customer ID " . strip_tags($cust) . "</p>";
$content .="<p>Job Site " . strip_tags($model) . "</p>";
$content .="<p>" . strip_tags($work) . "</p>";
$content .="<p>Sub Total: $ " . strip_tags($sub) . "</p>";
$content .="<p>Tax: $ " . strip_tags($tax) . "</p>";
$content .="<p>Total: $ " . strip_tags($total) . "</p>";
$content .="<p>Pay Online:  <a href=\"http://$comp_payUrl\" target=\"_blank\">$comp_payUrl</a></p>";
$content .="<em>" . strip_tags($comp_slogan) . "</em>";
$content .="</body></html>"; 

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
// Create email headers
$headers .= 'From: '.$from."\r\n".
    'Reply-To: '.$from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
 
$message = $content;
 
    // Sending email
    if(mail($to, $subject, $message, $headers)){
    echo 'Your mail has been sent successfully.';
    } else {
    echo 'Unable to send email. Please try again.';
    }
}
?>
<a href="../index.php">Dashboard</a>
<?php
include('../footer-print.php');
?>