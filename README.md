# CG-INVOICE #

Invoicing and Contacts program accessed from your web browser.

### What is this repository for? ###

* Print from preview window.
* Bootstrap HTML5 - PDO - MySQL - PHP
* Log in secure sessions
* Menu driven selections to create new invoice and display current invoices.
* Adaptable CSS styles and print page calls for printing.
* Scheduler calendar and vendor, client, contacts directory
* Email invoice directly from page without exporting file
* Version 2.01
* [Demo](https://larryjudd.us/cg-invoice)

### How do I get set up? ###

Must be installed on a server with PHP 5.2 or greater.

* Create database called cginvoice (for example)
* Extract files to folder on Apache Server/Linux/localhost
* Add tables to your database (PHPMyAdmin) from file ..../inc/cginvoice.sql.txt
* Add your database access information into ..../inc/config.php
* Navigate to ..../inc/regitration.php and register a new user. (LINK ALSO ON LOGIN PAGE)
* Add your company address and info into ....print-settings.php
* Put backlink to tradesouthwest.com somewhere on the net!

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Tradesouthwest http://tradesouthwest.com
* support@tradesouthwest.com