<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="private restricted no follow">
    <meta name="author" content="Larry Judd Oliver">
    <title>Admin Invoicing | Invoice</title>
    <!-- Bootstrap Core CSS -->
    <link href="inc/css/bootstrap.min.css" rel="stylesheet">
    <link href="printstyles.css" rel="stylesheet">
    <style type="text/css">
@media print {
    body {font: fit-to-print; font-family: sans-serif; color: #000; background: #fff;}
    .print-noprint { display: none; }
 @page { margin: 0; }
  body { margin: .6cm; }
    #printbody input[type='text'], #printbody input[type='number'], #printbody input[type='tel'], #printbody textarea {
        background: #fff;font-family: sans-serif;font-size: inherit;color: #111111 !important;border: 0 !important;
        border-style: none !important;}
    tr.nb, td.nb, .nb { border: none !important; border-bottom: 0px solid transparent;border-top: 0px solid transparent;}
    th{ padding-left: 1em;}
    input, textarea, .cleanprt {background: transparent;box-shadow: none !important;font-family: sans-serif;border-color: #fff !important;
    border: 0 !important;border-style: none !important;}
    #printbody {margin: 0 auto;  border: 1px solid #bbb; padding: 20px;}
    #printbody td {min-width: 48%; margin: 1px 0; padding: 1px 1%;}
    #printbody textarea {border: none;background: transparent; font-size: 1em !important;font-weight: normal;line-height: 1;margin: 3px 0;}
    
    .invoice-upper-section figure {position: relative; top: 0;margin-bottom: .25em;}
    .boxed {border: thin solid #ddd;}
    small{font-size: 12px; margin-right: 1em;}
    #print-noprint { display: none; }
    .printable { width: 100%; margin: 0; }
    .table-responsive { margin-top: -22px; }
    #text_wrap { max-width: 98.8992%; }
    .quote-print label { text-align: left; font-weight: 700; margin-right: 10px; width: 120px; }
    .quote-print p { width: 100%; clear: both; margin-bottom: 1em; }
    .end { text-align: left; border: thin solid #ddd; margin: -10px 20px 10px 0;}
    .printable-header { position: relative; top: 0; overflow: hidden; width: 100%; height: 130px; border-bottom: thin solid #ddd; display: inline-block; }
    .quote-print { margin: 0 auto;  border: 1px solid #bbb; padding: 20px;}
    .print-right { max-width: 45%; position: relative; top: -6.666666669em; float: right; }
}

/* page styles */
#printbody input[type='text'],  #printbody input[type='number'],  #printbody input[type='tel'],  #printbody textarea {
        background: #fff; font-family: sans-serif; font-size: inherit; color: #111111 !important; border: 0 !important;
        border-style: none !important; } 

#printbody {margin: 0 auto; margin-top: 20px; border: 1px solid #bbb; padding: 20px;}

#printbody td { margin: 10px 0; padding: 5px;}
#printbody textarea {border: none;background: transparent; font-size: 1em !important;font-weight: normal;line-height: 1;margin: 3px 0;
        width: 100%; height: 100%; overflow: visible;}

#printbody figure {position: relative;top: 0;margin-bottom: .25em;}
.boxed {border: thin solid #ddd;}
small{font-size: 12px; margin-right: 1em;}
table.subtable  { width: 100%; border: thin solid #ddd;}

table.subtable tr { border-bottom: thin dashed #bbb; }
table.subtable tr td:first-child { text-align: left; font-weight: 700; }
table.subtable tr td:last-child { text-align: right; }
.quote-print p { width: 100%; clear: both; margin-bottom: 1em;}
.end { float: right; text-align: left; border: thin solid #ddd; }
.printable-header { position: relative; top: 0; width: 100%; overflow: hidden; box-sizing: border-box; border-bottom: thin solid #ddd; height: 130px; }
.quote-print { margin: 0 auto;  border: 1px solid #bbb; padding: 20px;}
</style>

    <!-- Custom Fonts 
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="container-fluid">
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
            <a class="navbar-brand" href="index.php">Invoice Admin Home</a>
    </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Dashboard</a></li>
            <li><a href="">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a class="btn btn-info" href="inc/logout.php">Log Out</a></li>
          <li>
              <form class="navbar-form">
                  <input type="text" class="form-control" placeholder="Search...">
              </form>
          </li>
          </ul>
        </div>
</nav>
</div>

<div class="page-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
        
                <?php include('side-menu.php'); ?>

            </div>
        
                <div class="col-sm-9">