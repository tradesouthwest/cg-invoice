-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 04, 2015 at 12:25 AM
-- Server version: 5.6.23
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `larryjud_cginv`
--

-- --------------------------------------------------------

--
-- Table structure for table `cgadmins`
--

CREATE TABLE IF NOT EXISTS `cgadmins` (
  `admin_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `admin_username` varchar(40) NOT NULL DEFAULT '',
  `admin_password` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cgadmins`
--

INSERT INTO `cgadmins` (`admin_id`, `admin_username`, `admin_password`) VALUES
(1, 'admin', 'pass');

-- --------------------------------------------------------

--
-- Table structure for table `cgclients`
--

CREATE TABLE IF NOT EXISTS `cgclients` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `item` varchar(80) NOT NULL,
  `code` varchar(60) NOT NULL,
  `value` varchar(60) NOT NULL,
  `upc` varchar(125) NOT NULL,
  `din` varchar(20) NOT NULL,
  `dout` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cgclients`
--

INSERT INTO `cgclients` (`id`, `item`, `code`, `value`, `upc`, `din`, `dout`) VALUES
(1, 'johnnywalkerpop.com', 'on pacificbuilders.com linuxvps.net host', 'HTML5 static site Bootstrap new theme XYZ', 'login johnnywa pass #HiJkLmN0P', '2015-09-01', '2015-09-02');

-- --------------------------------------------------------

--
-- Table structure for table `cgcontacts`
--

CREATE TABLE IF NOT EXISTS `cgcontacts` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `first` varchar(25) NOT NULL,
  `last` varchar(25) NOT NULL,
  `address` varchar(125) NOT NULL,
  `phone` varchar(65) NOT NULL,
  `mobile` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `website` varchar(65) NOT NULL,
  `comment` varchar(250) NOT NULL,
  `date` varchar(65) NOT NULL,
  `category` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cgcontacts`
--

INSERT INTO `cgcontacts` (`id`, `first`, `last`, `address`, `phone`, `mobile`, `email`, `website`, `comment`, `date`, `category`) VALUES
(1, 'Dave', 'Thorenson', '301 E Ash Ave\r\nCasa Grande, AZ 85122', '602-555-1111', '800-555-1234', 'dave@jackofallmedia.org', 'http://jackofallmedia.com', '', '2015-09-02', 0),
(2, 'Larry', 'Oliver', '1444 W. Gazink\r\nPhoenix, AZ\r\n85029', '520-555-1234', '', 'support@tradesouthwest.com', 'http://tradesouthwest.com', 'comments go here', '2015-09-02', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cgevents`
--

CREATE TABLE IF NOT EXISTS `cgevents` (
  `event_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `event_day` int(2) NOT NULL DEFAULT '0',
  `event_month` int(2) NOT NULL DEFAULT '0',
  `event_year` int(4) NOT NULL DEFAULT '0',
  `event_time` varchar(5) NOT NULL DEFAULT '',
  `event_title` varchar(200) NOT NULL DEFAULT '',
  `event_desc` text NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cgevents`
--

INSERT INTO `cgevents` (`event_id`, `event_day`, `event_month`, `event_year`, `event_time`, `event_title`, `event_desc`) VALUES
(1, 3, 9, 2015, '12:00', 'New Event', 'Get there on time');

-- --------------------------------------------------------

--
-- Table structure for table `cginvoice`
--

CREATE TABLE IF NOT EXISTS `cginvoice` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  `phone` varchar(16) DEFAULT NULL,
  `address` varchar(225) DEFAULT NULL,
  `date` varchar(30) DEFAULT NULL,
  `invnum` varchar(25) DEFAULT NULL,
  `cust` varchar(12) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `work` text NOT NULL,
  `sub` int(7) DEFAULT NULL,
  `tax` int(7) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `paid` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cginvoice`
--

INSERT INTO `cginvoice` (`id`, `name`, `phone`, `address`, `date`, `invnum`, `cust`, `model`, `work`, `sub`, `tax`, `total`, `paid`, `status`) VALUES
(1, 'Billy Bob', '602-555-5555', '1234 What A St\r\nPhoenix, AZ 85009', '11/22/2013', '112213-2', 'counter', 'clone', 'needs repair of noisy gate', 0, 0, 0, '', 0),
(2, 'Billy Bob Nostrole', '415-555-2222', '1000 E. Samuel Dr\r\nJacksonville, HI', '2015-08-31', '83129-1', '', 'Sale', '1 - blue widget               $5\r\n3 - green widgets           $15\r\n2 - giant widgets             $20\r\n', 40, 0, 40, 'paid 8/31/2015', 0),
(3, 'Billy Bob Nostrole', '415-555-2222', '1000 E. Samuel Dr\r\nJacksonville, HI', '2015-09-03', '83129-1', '', 'Sale', '1 - blue widget               $5\r\n3 - green widgets           $15\r\n2 - giant widgets             $20\r\n', 40, 0, 40, 'paid 8/31/2015', 1),
(4, 'Billy Bob Nostrole', '415-555-2222', '1000 E. Samuel Dr\r\nJacksonville, HI', '2015-09-03', '83129-1', '', 'Sale', '1 - blue widget               $5\r\n3 - green widgets           $15\r\n2 - giant widgets             $20\r\n', 40, 0, 40, 'paid 9/3/2015', 2);

-- --------------------------------------------------------

--
-- Table structure for table `cgquote`
--

CREATE TABLE IF NOT EXISTS `cgquote` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  `phone` varchar(16) DEFAULT NULL,
  `address` varchar(225) DEFAULT NULL,
  `date` varchar(30) DEFAULT NULL,
  `invnum` varchar(25) DEFAULT NULL,
  `work` text NOT NULL,
  `sub` int(7) DEFAULT NULL,
  `tax` int(7) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `paid` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cgquote`
--

INSERT INTO `cgquote` (`id`, `name`, `phone`, `address`, `date`, `invnum`, `work`, `sub`, `tax`, `total`, `paid`, `status`) VALUES
(1, 'Quote Person', '', '1234 Anywhere', '2015-09-03', '', '2 thingys', 0, 0, 44, '', 0),
(2, 'Quote Person', '', '1234 Anywhere', '2015-09-03', '9032015-Q3', '2 thingys 10.00\r\n1 big thingy 20.00\r\n3 green thingy 30.00', 60, 0, 60, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cgvendors`
--

CREATE TABLE IF NOT EXISTS `cgvendors` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `business` varchar(80) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `hours` varchar(500) NOT NULL,
  `address` varchar(125) NOT NULL,
  `note` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cgvendors`
--

INSERT INTO `cgvendors` (`id`, `business`, `phone`, `hours`, `address`, `note`) VALUES
(1, 'National Widget Distributors', '602-555-1111', 'blue widgets 7.95\r\ngreen widgets 6.95\r\ngiant widgets 11.95\r\nlittle widget 4.95\r\ntry new entry\r\n', '1444 N. Some St. Phoenix, AZ 85055', '12prcnt discount on bulk and then some');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `salt` char(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `email`) VALUES
(1, 'admin', '915064208a81ea5da3147c144371b4e20551931cc69ad2577840d5ba6e9ea97c', 'a7d43b75a9a35ba', 'admin@nomail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
